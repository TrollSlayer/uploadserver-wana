import json
import base64

from flask import Flask
from flask import request, abort

server = Flask(__name__)


@server.route("/upload", methods=["POST"])
def upload():
    data = json.loads(request.data.decode('UTF-8'))
    nf = open("uploads/" + data['name'], 'wb+')
    nf.write(base64.b64decode(data['data']))
    nf.close()

    return ''


@server.route("/", methods=["POST"])
def login():
    cred = json.loads(request.data.decode('UTF-8'))
    if admin_cred(cred['user'], cred['pssw']):
        return ""
    else:
        abort(401)


# Verifica as credenciais (hardcoded)
def admin_cred(user, pssw):
    if user == 'admin' and pssw == 'admin':
        return True
    else:
        return False


def start_server():
    server.run(use_reloader=False)

