# QtUpload Server

#### Instalar dependências do Python
* PyQt5
* requests
* flask

#### Executar
```shell script
python3 start.py
```
Tanto o servidor Http quanto a GUI serão iniciadas, estando o servidor rodando em uma thread separada

![](assets/screen1.png)
![](assets/screen2.png)
![](assets/screen3.png)
