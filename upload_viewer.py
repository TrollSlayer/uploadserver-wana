import _thread
import sys
from time import sleep

import requests
import json
import os
import base64
import threading
import datetime

from PyQt5.QtCore import QEventLoop, pyqtSignal
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon
from PyQt5.QtWidgets import QApplication, QSystemTrayIcon, QMenu, QAction, QMainWindow
from PyQt5 import QtWidgets, uic

import start


class UploadThread(threading.Thread):
    def __init__(self, lista, trigger, label_progresso):
        super(UploadThread, self).__init__()
        self.lista = lista
        self._stop_event = threading.Event()
        self.cancelar = False
        self.trigger = trigger
        self.status = label_progresso

    def stop(self):
        self.cancelar = True

    def run(self):
        if self.lista:
            byte_count = 0
            for f in self.lista:
                data = open(f, 'rb').read()
                data = base64.b64encode(data)
                byte_count += len(data)

            for f in self.lista:

                if self.cancelar:
                    break

                data = open(f, 'rb').read()
                data = base64.b64encode(data)

                file_bytes = len(data)

                pack = {
                    "name": os.path.basename(f),
                    "data": data.decode('UTF-8')
                }

                start = datetime.datetime.now()

                requests.post(url="http://localhost:5000/upload", data=json.dumps(pack))

                byte_count -= file_bytes

                end = datetime.datetime.now()
                delta = end - start

                transfer_ratio = file_bytes / delta.total_seconds()
                et = round(byte_count / transfer_ratio, 0)

                if et < 1:
                    et = '<1'

                self.status.setText('Tempo estimado de conclusão: ' + str(et) + ' segundos')

                self.trigger.emit()

        return 0


class ProgressWindow(QtWidgets.QMainWindow):
    progress_trigger = pyqtSignal()

    def __init__(self, lista, parent=None, cred=None):
        super(ProgressWindow, self).__init__(parent)
        self.ui = uic.loadUi("ui/progress.ui")

        self.lista = lista
        self.ui.botao_cancelar.clicked.connect(self.setup)
        self.progress_trigger.connect(self.update_progresso)

        self.cada = 100 / len(self.lista)
        self.progresso = 0.0

        self.ui.show()

    def update_progresso(self):
        self.progresso += self.cada
        if self.progresso == 100:
            self.ui.hide()
        else:
            self.ui.barra_progresso.setValue(self.progresso)

    def setup(self):
        self.ui.botao_cancelar.setText("Cancelar")
        self.ui.botao_cancelar.clicked.disconnect()
        self.ui.botao_cancelar.clicked.connect(self.cancelar)

        self.do()

    def cancelar(self):
        self.thread.stop()
        self.ui.hide()
        return 0

    def do(self):
        self.thread = UploadThread(self.lista, self.progress_trigger, self.ui.label_status)
        self.thread.start()


class UploadWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None, cred=None):
        self.paths = []

        super(UploadWindow, self).__init__(parent)
        self.ui = uic.loadUi("ui/upload_view.ui")

        self.ui.botao_abrir.clicked.connect(self.abrir_arquivos)
        self.ui.botao_upload.clicked.connect(self.fazer_upload)
        self.lista = self.ui.listView

        self.ui.show()

    def fazer_upload(self):
        ProgressWindow(self.paths, parent=self)

    def abrir_arquivos(self):
        paths = QtWidgets.QFileDialog.getOpenFileNames(self, 'Escolher Arquivos')
        self.paths = list(paths[0])

        model = QStandardItemModel()
        for f in self.paths:
            model.appendRow(QStandardItem(f))

        self.lista.setModel(model)


class LoginWindow(QMainWindow):
    def __init__(self, parent=None):
        super(LoginWindow, self).__init__(parent)
        self.ui = uic.loadUi("ui/login.ui")

        self.ui.botao_login.clicked.connect(self.fazer_login)

    def show(self):
        self.ui.show()

    def fazer_login(self):
        cred = {
            "user": self.ui.campo_user.text(),
            "pssw": self.ui.campo_senha.text()
        }

        response = requests.post(url="http://localhost:5000/", data=json.dumps(cred))

        if response.status_code == 200:
            self.ui.hide()
            UploadWindow(self, cred=cred)


class TrayApp(QSystemTrayIcon):
    def __init__(self):
        QSystemTrayIcon.__init__(self)
        self.icon = QIcon("assets/icon.png")

        self.setIcon(self.icon)
        self.setVisible(True)

        self.menu = QMenu()

        self.abrir = QAction("Abrir")
        self.abrir.triggered.connect(show_login)
        self.menu.addAction(self.abrir)

        self.action = QAction("Fechar")
        self.action.triggered.connect(kill_all)
        self.menu.addAction(self.action)

        self.setContextMenu(self.menu)
        self.show()


app = QApplication(sys.argv)
login = LoginWindow()

def start_gui():
    app.setQuitOnLastWindowClosed(False)

    show_login()

    tray = TrayApp()
    sys.exit(app.exec_())


def show_login():
    login.show()


def kill_all():
    os._exit(0)
