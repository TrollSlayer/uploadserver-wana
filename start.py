from PyQt5.QtCore import QEventLoop

import server
import upload_viewer

import threading

def startup():
    def routine_server():
        server.start_server()

    thread = threading.Thread(target=routine_server)
    thread.start()

if __name__ == '__main__':
    startup()
    upload_viewer.start_gui()

